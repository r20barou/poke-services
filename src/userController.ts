import { User } from './model'
import UserRepository from './userRepository'
import users from './users.json'

const dbUsers: UserRepository = new UserRepository();

const listUsers = () => {
   return dbUsers.getAllUsers()
}
  
const addUser = (name: string, password: string) => {
   dbUsers.createUser(name,password);
   return dbUsers.getAllUsers()
}

const connect = (name: string, password: string) => {
    return dbUsers.connect(name,password);
}
  
export { listUsers, addUser, connect }
