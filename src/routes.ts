import * as express from "express"
import * as UserController from "./userController"
import { User } from './model'

export const register = ( app: express.Application ) => {
  app.get('/', (req, res) => res.send('Hello World!'));

  app.get('/user', (req, res) => {
	res.status(200).json(UserController.listUsers())
  })
 
  app.put('/user/register', (req, res) => {
	const newUser: User = req.body    
	res.status(200).json(UserController.addUser(newUser.name,newUser.password))
  })
  
  app.post('/user/connect', (req, res) =>{
    const user: User = req.body;
    res.status(200).json(UserController.connect(user.name,user.password))
  });

  app.get('/')
}
