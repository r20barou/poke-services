"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connect = exports.addUser = exports.listUsers = void 0;
const userRepository_1 = __importDefault(require("./userRepository"));
const dbUsers = new userRepository_1.default();
const listUsers = () => {
    return dbUsers.getAllUsers();
};
exports.listUsers = listUsers;
const addUser = (name, password) => {
    dbUsers.createUser(name, password);
    return dbUsers.getAllUsers();
};
exports.addUser = addUser;
const connect = (name, password) => {
    return dbUsers.connect(name, password);
};
exports.connect = connect;
//# sourceMappingURL=userController.js.map